/* eslint-disable */

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import router from '../router/index'
Vue.use(Vuex)

let dayjs = require('dayjs')
let customParseFormat = require('dayjs/plugin/customParseFormat')
dayjs.extend(customParseFormat)

axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

export default new Vuex.Store({
  state: {
    error: '', // error: {header: header, description: description, level: info | success | error}
    is_reg: localStorage.getItem('isReg'),
    login: localStorage.getItem('login'),
    password: localStorage.getItem('password'),
    group_name: localStorage.getItem('group_name'),
    group_id: localStorage.getItem('group_id'),
    date: dayjs().format('YYYY-MM-DD'),
    tasks: [],
    group_user_list: [],
    user_id: localStorage.getItem('user_id')
  },
  mutations: {
    SET_ERROR: (state, payload) => {
      state.error = payload
    },
    SET_IS_REG: (state, payload) => {
      state.is_reg = payload
      localStorage.setItem('isReg', payload)
    },
    SET_LOGIN: (state, payload) => {
      state.login = payload
      localStorage.setItem('login', payload)
    },
    SET_PASSWORD: (state, payload) => {
      state.password = payload
      localStorage.setItem('password', payload)
    },
    SET_TASKS: (state, payload) => {
      state.tasks = payload
    },
    SET_DATE: (state, payload) => {
      state.date = payload
    },
    INCREMENT_DATE: (state) => {
      state.date = dayjs(state.date, 'YYYY-MM-DD').add(1, 'day').format('YYYY-MM-DD')
    },
    DECREMENT_DATE: (state) => {
      state.date = dayjs(state.date, 'YYYY-MM-DD').add(-1, 'day').format('YYYY-MM-DD')
    },
    SET_GL_LIST: (state, payload) => {
      state.group_user_list = payload
    },
    SET_GR_ID: (state, payload) => {
      state.group_id = payload
      localStorage.setItem('group_id', payload)
    },
    SET_GR_NAME: (state, payload) => {
      state.group_name = payload
      localStorage.setItem('group_name', payload)
    },
    SET_USER_ID: (state, payload) => {
      state.user_id = payload
      localStorage.setItem('user_id', payload)
    }
  },
  getters: {
    ERROR: state => {
      return state.error
    },
    DATE: state => {
      return state.date
    },
    IS_REG: state => {
      return state.is_reg
    },
    GROUP_NAME: state => {
      return state.group_name
    },
    GROUP_ID: state => {
      return state.group_id
    },
    PASSWORD: state => {
      return state.password
    },
    LOGIN: state => {
      return state.login
    },
    TASKS: state => {
      return state.tasks
    },
    GR_LIST: state => {
      return state.group_user_list
    }
  },
  actions: {
    REGISTER_USER: (context, payload) => {
      axios.put(`http://diarya.rusm.site/api/user/`, {
        login: payload.name,
        password: payload.password,
      }).then(() => {
        context.commit("SET_LOGIN", payload.name)
        context.commit("SET_PASSWORD", payload.password)
        context.commit('SET_IS_REG', 'true')
        router.push("/tasks")
      }).catch(() => {
        context.commit("SET_ERROR", {
          header: "This name already taken.",
          description: "",
          level: "error",
        })
        setTimeout(() => context.commit('SET_ERROR', ''), 3000)
      })
    },
    LOGIN_USER: (context, payload) => {
      axios.post(
        `http://diarya.rusm.site/api/user/`,
        {
          login: payload.name,
          password: payload.password,
        }, {withCredentials: true}
      ).then((res) => {
          context.commit("SET_LOGIN", payload.name)
          context.commit("SET_PASSWORD", payload.password)
          context.commit('SET_IS_REG', 'true')
          context.commit('SET_GR_ID', res.data.group_id)
          context.commit('SET_GR_NAME', res.data.group_name)
          context.commit('SET_USER_ID', res.data.user_id)
          router.push("/tasks")
      }).catch(() => {
          context.commit("SET_ERROR", {
          header: "You\'re login or password is incorrect",
          description: "",
          level: "error",
        })
        setTimeout(() => context.commit('SET_ERROR', ''), 3000)
      })
    },
    SIGN_OUT: (context, payload) => {
      context.commit("SET_LOGIN", '')
      context.commit("SET_PASSWORD", '')
      context.commit('SET_IS_REG', '')
      router.push('/')
      axios.delete('http://diarya.rusm.site/api/user/', { withCredentials: true })
    },
    CREATE_TASK: ({context, state}, payload, ) => {
      axios.post(`http://diarya.rusm.site/api/${state.group_id}`)
    },
    GET_TASKS: ({commit, state, dispatch}) => {
      if (state.group_id) {
        axios.get(`http://diarya.rusm.site/api/tasks/${state.group_id}/${state.date}`, {withCredentials: true}).then(res => {
          commit('SET_TASKS', res.data.tasks)
        }).catch(error => {
          commit("SET_ERROR", {
            header: "You're not login.",
            description: "",
            level: "error"
          })
          setTimeout(() => commit('SET_ERROR', ''), 3000)
        })
      } else {
        commit("SET_ERROR", {
          header: "You must set the group.",
          description: "Visit you're profile page",
          level: "error"
        })
        setTimeout(() => commit('SET_ERROR', ''), 3000)
      }
    },
    GET_GR_LIST: ({commit, state, dispatch}) => {
      axios.get(`http://diarya.rusm.site/api/users/${state.group_id}`).then(res => {
        commit('SET_GL_LIST', res.data.users)
      }).catch({})
    },
    ADD_VK_ID: ({commit, state, dispatch}, payload) => {
      axios.post(`http://diarya.rusm.site/api/user/${state.user_id}`, {
        vk_id: payload
      })
    }
  }
})
